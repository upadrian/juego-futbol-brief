# Juego mete-gol para stand o acción web

El juego arranca con una pantalla de bienvenida (logos, descripción de juego, etc).
El fondo de esta pantalla, muestra el video del golero en espera.

Cuando el jugador quiere jugar, se puede mostrar una de dos pantallas:

1. Registro de datos (formulario y botón de registro)
2. Sin registro de datos (botón de "empezar")

Después de registrarse(o no), la pantalla muestra al golero expectante y un cartel que anima al jugador a jugar.

La tribuna está animada, y puede tener la publicidad de la marca en la marquesina. Ver nota sobre las opciones de branding.

> (V1) Golero esperando. Se ve el arquero esperando el penal y la pelota en el punto penal.



**Opción "Pericia del jugador":**

  Cuando el jugador empieza la partida, se muestra una flecha que bascula a diferentes velocidades de forma horizontal.
 
  El jugador entonces clickea(o toca una tecla o el sensor lo capta) y la flecha marca la "dirección" de la pelota.
  
  La dirección son 4 posibles(se detectan pero no se ven), L1, L2, R2, R1.
  
  A continuación esta flecha se va y aparece otra, que bascula de forma vertical, indicando mayor o menor "fuerza".

  La fuerza tiene 4 sectores (no se ven, se detectan), F1, F2, F3, F4. Si es mucha(F4) o poca(F1), el usuario tiene mayor posibilidad de errar.
  
  Cuando el jugador clickea, se obtiene la fuerza, y con esta se decide si mete o no el gol.
  

**Opción "Aleatorio"**
  
  Se muestra un cartel animando al jugador, el cual ejecuta la acción (click, tecla o sensor)
  
  La fuerza y la dirección se calculan de forma aleatoria cuando el jugador acciona
   

_Después de que se obtiene si gana o no, se dispara uno de los siguientes videos:_ 

* L1: pelota al piso, sobre la izquierda.
  *  (V2) Video mete gol piso a la izquierda
  *  (V3) Video erra y va al palo, al piso a la izquierda
* L2: pelota a la esquina superior izquierda del arco:
  *  (V4) Video mete gol en la esquina izquierda
  *  (V5) Video erra y va afuera, esquina izquierda
* R2: pelota a la esquina superior derecha del arco:
  * (V6) Video mete gol en la esquina derecha
  * (V7) Video erra y va afuera, esquina derecha
* R1: pelota al piso, sobre la derecha
  * (V8) Video mete gol piso a la derecha
  * (V9) Video erra y va al palo, al piso a la derecha


## Dinámica

Si la fuerza es normal(F2-F3), tiene mayor posibilidad de meter el gol.

## Videos 

9 videos, 8 de acciones y 1 del jugador en espera.

Resolución 1920x1080, formato MP4 o MOV. 

Todos los videos de acciones tienen que tener la  misma duración y empezar y terminar en el mismo frame (para unir las animaciones)

## Branding

Se puede integrar al video de fondo, en la marquesina la marca.

Se pueden agregar elementos de la marca, en diferentes lugares.

## Portabilidad

El juego funciona siempre de forma apaisada, ya que la relación de aspecto de los videos es apaisada.

En celulares, el juego se vera "chico"

La resolución estandar del juego, es para una pantalla de 1920x1080
